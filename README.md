# streaming-web

A simple webinterface for streaming audio live from the corresponding server.

Relies on webassembly support for opus and samplerate conversion and hence needs emscripten installed.

## Building

This project needs to compile `libopus` and `libsamplerate` before the website can be built.

Running `make` should achieve all of this.

## Contributors

 - Frederick Gnodtke
