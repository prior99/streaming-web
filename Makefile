default: build

opus/.libs:
	cd opus; ./autogen.sh; emconfigure ./configure --disable-rtcd --disable-intrinsics --disable-shared --enable-static; emmake make -j$(nproc)

libsamplerate/.libs:
	cd libsamplerate; ./autogen.sh; emconfigure ./configure --disable-rtcd --disable-intrinsics --disable-shared --enable-static; emmake make -j$(nproc)

.PHONY: native
native: libsamplerate/.libs opus/.libs

node_modules:
	yarn

.PHONY: run
run: native node_modules
	yarn start

.PHONY: lint node_modules
lint: native
	yarn lint

.PHONY: build
build: native node_modules
	yarn build
