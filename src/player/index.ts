import { observable, computed, reaction } from "mobx";
import { SourceNode } from "./source-node";
import { component } from "tsdi";
import { Message } from "../worker/messages";

const AUDIO_CTX_BUFFER_SIZE = 8192 * 2;

@component
export class Player {
    private worker: Worker;
    private source: SourceNode;
    @observable public url: string;
    private audioCtx: AudioContext;
    @observable public sampleRate: number;
    @observable public channels: number;
    public analyser: AnalyserNode;
    public gain: GainNode;
    @observable public animationEnabled = true;
    @observable public volume = 100;

    public start(url: string, audioCtx: AudioContext) {
        this.url = url;
        this.audioCtx = audioCtx;
        this.analyser = audioCtx.createAnalyser();
        this.analyser.fftSize = 2048;
        this.gain = audioCtx.createGain();
        this.worker = new Worker("/worker.js");
        this.worker.addEventListener("message", ({ data }) => this.onMessage(data));
        this.worker.postMessage({
            action: "start",
            url,
            sampleRate: audioCtx.sampleRate,
        });
        reaction(() => this.volume, volume => this.gain.gain.value = volume / 100);
        this.volume = 50;
    }

    @computed public get started() {
        return Boolean(this.url);
    }

    @computed public get initialized() {
        return Boolean(this.sampleRate) && Boolean(this.channels);
    }

    public disableAnimation() {
        this.analyser.disconnect();
        this.gain.connect(this.audioCtx.destination);
        this.animationEnabled = false;
    }

    public enableAnimation() {
        this.gain.disconnect();
        this.gain.connect(this.analyser);
        this.analyser.connect(this.audioCtx.destination);
        this.animationEnabled = true;
    }

    private async initialize(sampleRate: number, channels: number) {
        this.source = new SourceNode(AUDIO_CTX_BUFFER_SIZE, channels, this.audioCtx);
        this.source.connect(this.gain);
        if (this.animationEnabled) { this.enableAnimation(); }
        else { this.disableAnimation(); }
        this.sampleRate = sampleRate;
        this.channels = channels;
    }

    private onMessage(data: Message) {
        switch (data.action) {
            case "audio":
                this.source.appendPCM(data.data);
                break;
            case "info":
                this.initialize(data.sampleRate, data.channels);
                break;
            default:
                break;
        }
    }

    public stop() {
        this.url = undefined;
        this.sampleRate = undefined;
        this.channels = undefined;
        this.audioCtx = undefined;
        this.source.disconnect();
        this.worker.postMessage({
            action: "stop",
        });
    }
}
