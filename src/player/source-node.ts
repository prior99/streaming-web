import { AudioBuffer } from "./audio-buffer";

/**
 * An `AudioNode` wrapper generating audio from decoded PCM chunks.
 */
export class SourceNode {
    private buffers: AudioBuffer[] = [];
    public processor: ScriptProcessorNode;

    constructor(bufferSize: number, private channels: number, audioCtx: AudioContext) {
        this.processor = audioCtx.createScriptProcessor(bufferSize, channels);
        this.processor.addEventListener("audioprocess", this.onAudioProcess.bind(this));
        for (let i = 0; i < channels; ++i) {
            this.buffers.push(new AudioBuffer(bufferSize));
        }
    }

    /**
     * Append new PCM data into the buffer.
     */
    public appendPCM(data: Float32Array[]) {
        data.forEach((channelData, index) => this.buffers[index].append(channelData));
    }

    /**
     * Connect the underlying `ScriptProcessorNode` to a destination.
     */
    public connect(node: AudioNode) {
        this.processor.connect(node);
    }

    /**
     * Disconnect the underlying `ScriptProcessorNode`.
     */
    public disconnect() {
        this.processor.disconnect();
    }

    private onAudioProcess({ outputBuffer }: AudioProcessingEvent) {
        // Deinterleave the data and hand it over to the `AudioContext`.
        for (let i = 0; i < this.channels; ++i) {
            const slice = this.buffers[i].getSlice(outputBuffer.length);
            outputBuffer.getChannelData(i).set(slice);
        }
    }
}
