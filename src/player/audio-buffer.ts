/**
 * Store a continuous stream of incoming audio data,
 * and provide a function to retrieve a slice of it from the beginning.
 */
export class AudioBuffer {
    private remaining: Float32Array;
    private queue: Float32Array[] = [];
    private remainingLength = 0;

    constructor(private sliceSize: number) {
        this.remaining = new Float32Array(sliceSize);
    }

    /**
     * Append a new slice of PCM audio data to the end of the buffer.
     */
    public append(input: Float32Array) {
        for (let inputIndex = 0; inputIndex < input.length;) {
            let lengthToCopy = Math.min(this.sliceSize - this.remainingLength, input.length - inputIndex);
            this.remaining.set(input.slice(inputIndex, inputIndex + lengthToCopy), this.remainingLength);
            this.remainingLength += lengthToCopy;
            inputIndex += lengthToCopy;
            if (this.remainingLength === this.sliceSize) {
                this.queue.push(this.remaining.slice());
                this.remainingLength = 0;
            }
        }
    }

    /**
     * Retrieve and remove a slice of PCM audio data from the start of the buffer.
     */
    public getSlice(length: number) {
        if (this.queue.length > 0) { return this.queue.shift(); }
        return undefined;
    }

    public get length() {
        return this.queue.length;
    }
}
