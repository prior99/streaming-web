import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Room, createRoom } from "../api";
import * as css from "./help.scss";

@observer
export class Help extends React.Component {
    @observable private room: Room;

    constructor(props: {}) {
        super(props);
    }

    public async componentDidMount() {
        this.room = await createRoom();
    }

    public render() {
        if (!this.room) { return null; }
        return (
            <section className={css.help}>
                <article>
                    <h1>Help</h1>
                    <p>This is a utility for broadcasting audio while performing live from a <a href="https://en.wikipedia.org/wiki/Digital_audio_workstation">DAW</a> such as <a href="https://www.ableton.com/">Ableton</a>.</p>
                    <h2>1. Download</h2>
                    <p>A <a href="https://en.wikipedia.org/wiki/Virtual_Studio_Technology">VST plugin</a> you can <b><a href="https://gitlab.com/prior99/streaming-vst/-/jobs/artifacts/master/raw/target/x86_64-pc-windows-gnu/release/streamvst.dll?job=build">download here</a></b> will stream the audio live to a server.</p>
                    <p>The server will broadcast the audio to people that are using this page.</p>
                    <p />
                    <h2>2. Configure</h2>
                    <p>Install the VST plugin and place a <i>streamvst.yml</i> file in your <i>%AppData%</i> directory, containing the speaker URL (Grab a pair from the main page):</p>
                    <p><i>url: "wss://example.com/speak/1234567890"</i></p>
                    <h2>3. Done.</h2>
                    <p>Send the corresponding listening URL to your streamers and let them enjoy your music in the browser.</p>
                    <p className={css.ok}><a href="/">OK</a></p>
                </article>
            </section>
        );
    }
}
