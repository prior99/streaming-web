import * as React from "react";
import { observer } from "mobx-react";
import * as css from "./listen.scss";
import { Player } from "../player";
import { external, inject, initialize } from "tsdi";
import { BarBottom } from "./bar-bottom";
import { WaveForm } from "./waveform";
import { Volume } from "./volume";

declare const WEBSOCKET_BASE_URL: string;

interface ListenProps {
    match: {
        params: {
            id: string;
        };
    };
}

@external @observer
export class Listen extends React.Component<ListenProps> {
    @inject private player: Player;

    @initialize
    protected initialize() {
        this.player.start(`${WEBSOCKET_BASE_URL}/listen/${this.props.match.params.id}`, new AudioContext());
    }

    public render() {
        return (
            <section className={css.listen}>
                <WaveForm />
                <BarBottom />
                <Volume />
            </section>
        );
    }
}
