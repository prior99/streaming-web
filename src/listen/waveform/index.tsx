import * as React from "react";
import { observer } from "mobx-react";
import * as css from "./waveform.scss";
import { Player } from "../../player";
import { external, inject, initialize } from "tsdi";

@external @observer
export class WaveForm extends React.Component {
    @inject private player: Player;
    private ctx: CanvasRenderingContext2D;
    private width: number;
    private height: number;

    @initialize
    protected initialize() {
        this.draw();
    }

    private draw() {
        if (this.ctx && this.player.animationEnabled) {
            this.ctx.fillStyle = "rgba(26, 20, 20, 0.2)";
            this.ctx.fillRect(0, 0, this.width, this.height);
            const { analyser } = this.player;
            const timeDomain = new Float32Array(analyser.frequencyBinCount);
            const frequencies = new Float32Array(analyser.frequencyBinCount);
            analyser.getFloatTimeDomainData(timeDomain);
            analyser.getFloatFrequencyData(frequencies);
            const { index: maxFreqIndex } = frequencies.reduce((max, current, index) => {
                if (max === undefined || current > max.value) {
                    return { value: current, index };
                }
                return max;
            }, undefined as { value: number, index: number });
            const maxFreq = maxFreqIndex / frequencies.length;
            const maxAmplitude = Math.max(...timeDomain);
            this.ctx.lineWidth = 2;
            this.ctx.beginPath();
            // this.ctx.strokeStyle = "rgba(255, 255, 250, 0.5)";
            this.ctx.strokeStyle = `hsl(${maxFreq * 360}, 50%, ${maxAmplitude * 70 + 15}%)`;
            const sliceWidth = this.width / timeDomain.length;
            for (let i = 0; i < timeDomain.length; ++i) {
                const value = timeDomain[i];
                const y = this.height / 2 + value * (this.height / 2);
                const x = i * sliceWidth;
                if (i === 0) {
                    this.ctx.moveTo(x, y);
                } else {
                    this.ctx.lineTo(x, y);
                }
            }
            this.ctx.stroke();
        }
        requestAnimationFrame(this.draw.bind(this));
    }

    private renderLoop(canvas: HTMLCanvasElement) {
        if (!canvas) { return; }
        this.ctx = canvas.getContext("2d");
        const rect = canvas.getBoundingClientRect();
        this.width = rect.width;
        this.height = rect.height;
        canvas.width = rect.width;
        canvas.height = rect.height;
    }

    public render() {
        if (!this.player.initialized) { return null; }
        return (
            <canvas ref={this.renderLoop.bind(this)} className={css.canvas} />
        );
    }
}
