import * as React from "react";
import { observer } from "mobx-react";
import { action } from "mobx";
import * as css from "./volume.scss";
import { Player } from "../../player";
import { external, inject } from "tsdi";

@external @observer
export class Volume extends React.Component {
    @inject private player: Player;

    @action.bound private handleChange(evt: React.ChangeEvent<HTMLInputElement>) {
        this.player.volume = Number(evt.currentTarget.value);
        return;
    }

    public render() {
        if (!this.player.initialized) { return null; }
        return (
            <div className={css.volume}>
                <input type="range" min={1} max={100} value={this.player.volume} onChange={this.handleChange}/>
            </div>
        );
    }
}
