import * as React from "react";
import { observer } from "mobx-react";
import { action } from "mobx";
import * as css from "./bar.scss";
import { Player } from "../../player";
import { AudioInfo } from "./audio-info";
import { external, inject } from "tsdi";

@external @observer
export class BarBottom extends React.Component {
    @inject private player: Player;

    @action.bound private toggleAnimation() {
        if (this.player.animationEnabled) {
            this.player.disableAnimation();
        } else {
            this.player.enableAnimation();
        }
    }

    public render() {
        if (!this.player.initialized) { return null; }
        return (
            <div className={css.bar}>
                <div
                    className={`${css.button} ${this.player.animationEnabled ? css.active : ""}`}
                    onClick={this.toggleAnimation}
                >
                    Animation
                </div>
                <AudioInfo />
            </div>
        );
    }
}
