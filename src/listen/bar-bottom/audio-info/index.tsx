import * as React from "react";
import { observer } from "mobx-react";
import * as css from "./audio-info.scss";
import { Player } from "../../../player";
import { external, inject } from "tsdi";

@external @observer
export class AudioInfo extends React.Component {
    @inject private player: Player;

    private renderContent() {
        if (!this.player.started) {
            return (
                <>Connecting...</>
            );
        }
        if (!this.player.initialized) {
            return (
                <>Waiting for info...</>
            );
        }
        return (
            <>
                <p>Live</p>
                <p>{this.player.sampleRate}Hz</p>
                <p>{this.channels}</p>
            </>
        );
    }

    private get channels() {
        switch (this.player.channels) {
            case 1: return "Mono";
            case 2: return "Stereo";
            default: return `${this.player.channels} channels`;
        }
    }

    public render() {
        return (
            <div className={css.audioInfo}>
                {this.renderContent()}
            </div>
        );
    }
}
