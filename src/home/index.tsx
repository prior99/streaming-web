import * as React from "react";
import { observable } from "mobx";
import { observer } from "mobx-react";
import { Room, createRoom } from "../api";
import * as css from "./home.scss";

declare const WEBSOCKET_BASE_URL: string;

@observer
export class Home extends React.Component {
    @observable private room: Room;

    constructor(props: {}) {
        super(props);
    }

    public async componentDidMount() {
        this.room = await createRoom();
    }

    private get listenerUrl() {
        return `${location.href}listen/${this.room.listener}`;
    }

    private get speakerUrl() {
        return `${WEBSOCKET_BASE_URL}/speak/${this.room.speaker}`;
    }

    public render() {
        if (!this.room) { return null; }
        return (
            <section className={css.home}>
                <article>
                    <h1>Create a new room</h1>
                    <p><span>Speak:</span> {this.speakerUrl}</p>
                    <p><span>Listen:</span> <a href={this.listenerUrl}>{this.listenerUrl}</a></p>
                    <p className={css.help}><a href="help">???</a></p>
                </article>
            </section>
        );
    }
}
