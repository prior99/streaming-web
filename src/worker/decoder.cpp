#include <cstdint>
#include <iostream>
#include <vector>
#include <cmath>
#include <emscripten.h>
#include <opus.h>
#include <samplerate.h>

using namespace std;

class Decoder {
    public:
        Decoder(
            OpusDecoder *opus_decoder,
            SRC_STATE *samplerate_converter,
            uint32_t source_sample_rate,
            uint32_t target_sample_rate,
            uint32_t channels
        ) :
            source_sample_rate(source_sample_rate),
            target_sample_rate(target_sample_rate),
            channels(channels),
            opus_decoder(opus_decoder),
            samplerate_converter(samplerate_converter)
        {}

        /**
         * Analyze a chunk of opus data and return the number of samples contained in it.
         */
        int analyze_samples(const unsigned char *input, size_t input_length) {
            return opus_packet_get_nb_samples(input, input_length, this->source_sample_rate);
        }

        /**
         * Calculate samplerate ratio from source ratio to system ratio.
         */
        double ratio() {
            return this->target_sample_rate / static_cast<double>(this->source_sample_rate);
        }

        /**
         * Decode a chunk of opus data and perform a samplerate conversion to the system's sample rate.
         */
        int decode(const unsigned char *input, size_t input_length, float *output, size_t samples) {
            // Create an array for the decoder to write its output into.
            float *decoder_output = new float[samples * this->channels];
            // Decode the encoded opus audio into PCM.
            const int decoded_size = opus_decode_float(this->opus_decoder, input, input_length, decoder_output, samples, 0);
            // Perform samplerate conversion.
            SRC_DATA data;
            data.data_in = decoder_output;
            data.data_out = output;
            data.input_frames = samples;
            data.output_frames = ceil(this->ratio() * samples);
            data.src_ratio = this->ratio();
            src_process(this->samplerate_converter, &data);
            // Delete the memory chunk the decoder wrote to.
            delete[] decoder_output;
            return decoded_size;
        }

        ~Decoder() {
            // Cleanup opus decoder.
            if(this->opus_decoder) {
                opus_decoder_destroy(this->opus_decoder);
            }
            // Cleanup libsamplerate.
            if(this->samplerate_converter) {
                src_delete(this->samplerate_converter);
            }
        }

    private:
        uint32_t source_sample_rate;
        uint32_t target_sample_rate;
        uint32_t channels;
        OpusDecoder *opus_decoder;
        SRC_STATE *samplerate_converter;
};

extern "C" {
    EMSCRIPTEN_KEEPALIVE Decoder *createDecoder(uint32_t source_sample_rate, uint32_t target_sample_rate, uint32_t channels) {
        // Initialize opus.
        int opus_err;
        OpusDecoder *opus_decoder = opus_decoder_create(source_sample_rate, channels, &opus_err);
        if (opus_decoder == nullptr) {
            cerr << "Failed to initialize opus. ERR " << opus_err << "." << endl;
            return nullptr;
        }
        // Initialize libsamplerate.
        int samplerate_err;
        SRC_STATE *samplerate_converter = src_new(SRC_SINC_FASTEST, channels, &samplerate_err);
        if (samplerate_converter == nullptr) {
            cerr << "Failed to initialize libsamplerate. ERR " << samplerate_err << "." << endl;
            return nullptr;
        }
        // Initialize decoder.
        return new Decoder(opus_decoder, samplerate_converter, source_sample_rate, target_sample_rate, channels);
    }

    EMSCRIPTEN_KEEPALIVE int analyzeSamples(Decoder *decoder, const unsigned char *input, size_t length) {
        return decoder->analyze_samples(input, length);
    }

    EMSCRIPTEN_KEEPALIVE int decode(Decoder *decoder, const unsigned char *input, size_t input_length, float *output, size_t samples) {
        return decoder->decode(input, input_length, output, samples);
    }

    EMSCRIPTEN_KEEPALIVE void destroyDecoder(Decoder *decoder) {
        delete decoder;
    }
}
