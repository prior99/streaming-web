import { Message } from "./messages";
import { Worker } from "./worker";

let worker: Worker;

function handleMessage(message: Message) {
    switch (message.action) {
        case "start":
            const { url, sampleRate } = message;
            worker = new Worker(url, sampleRate);
            break;
        case "stop":
            worker.stop();
            break;
        default:
            break;
    }
}

onmessage =  evt => {
    handleMessage(evt.data);
};
