/**
 * Convert from an interleaved format into multiple arrays (one per channel).
 * Example: `[LRLRLRLRLR]` would be converted to `[LLLLL]`, `[RRRRR]`.
 */
export function deinterleave(buffer: Float32Array, channels: number): Float32Array[] {
    const result = [];
    const channelLength = buffer.length / channels;
    // Create empty target arrays to store deinterleaved audio data in.
    for (let i = 0; i < channels; ++i) {
        result[i] = new Float32Array(channelLength);
    }
    // Split the audio data.
    for (let i = 0; i < channelLength; ++i) {
        for (let channel = 0; channel < channels; ++channel) {
            result[channel][i] = buffer[i * channels + channel];
        }
    }
    return result;
}
