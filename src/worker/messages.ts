export interface Start {
    action: "start";
    url: string;
    sampleRate: number;
}

export interface Stop {
    action: "stop";
}

export interface Audio {
    action: "audio";
    data: Float32Array[];
}

export interface Info {
    action: "info";
    channels: number;
    sampleRate: number;
}

export type Message = Start | Stop | Audio | Info;
