import { Decoder, createDecoder } from "./decoder";
import { deinterleave } from "./deinterleave";

export class Worker {
    private channels: number;
    private socket: WebSocket;
    private decoder: Decoder;

    constructor(url: string, private targetSampleRate: number) {
        this.socket = new WebSocket(url);
        this.socket.addEventListener("message", this.onMessage.bind(this));
        this.socket.binaryType = "arraybuffer";
    }

    private async initialize(sampleRate: number, channels: number) {
        this.channels = channels;
        this.decoder = await createDecoder(sampleRate, this.targetSampleRate, channels);
        console.info(`Worker initialized with ${sampleRate}Hz samplerate and ${channels} channels.`);
        postMessage({
            action: "info",
            channels,
            sampleRate,
        });
    }

    private async onMessage({ data }: MessageEvent) {
        if (typeof data === "string") {
            // Parse new info.
            const { sample_rate: sampleRate, channels } = JSON.parse(data);
            // Delete old instances if necessary.
            if (this.decoder) { this.decoder.destroy(); }
            // Reinitialize.
            await this.initialize(sampleRate, channels);
        }
        else if (data instanceof ArrayBuffer) {
            if (!this.decoder) { return; }
            postMessage({
                action: "audio",
                data: deinterleave(this.decoder.decode(data), this.channels),
            });
        }
    }

    public stop() {
        this.socket.close();
        this.decoder.destroy();
        this.decoder = undefined;
    }
}
