import * as wasm from "./decoder.cpp";

let em: any;

/**
 * A higher-level interface for dealing with the wasm `Decoder` class.
 */
export class Decoder {
    private ptrDecoder: any; // A pointer to the wasm `decoder` class.

    constructor(private sourceSampleRate: number, private targetSampleRate: number, private channels: number) {
        this.ptrDecoder = em.exports.createDecoder(sourceSampleRate, targetSampleRate, channels);
        if (this.ptrDecoder === 0) {
            throw new Error("Unable to create decoder.");
        }
    }

    /**
     * Decode a chunk of opus data and perform a samplerate conversion into the system's sample rate.
     */
    public decode(input: ArrayBuffer): Float32Array {
        // Copy input array into Emscipten's heap.
        const ptrInput = em.exports.malloc(input.byteLength);
        const heapInput = new Uint8Array(em.emModule.HEAPU8.buffer, ptrInput, input.byteLength);
        heapInput.set(new Uint8Array(input));
        // Get output size.
        const samples = em.exports.analyzeSamples(this.ptrDecoder, heapInput.byteOffset, heapInput.byteLength);
        // console.log("Analysis yielded: ", samples);
        const outputLength = Math.ceil(samples * this.sampleRateRatio * this.channels);
        // Create output array in EmScripten's heap.
        const ptrOutput = em.exports.malloc(outputLength * Float32Array.BYTES_PER_ELEMENT);
        em.exports.decode(this.ptrDecoder, ptrInput, heapInput.byteLength, ptrOutput, samples);
        const heapOutput = new Float32Array(em.emModule.HEAPF32.buffer, ptrOutput, outputLength);
        em.exports.free(ptrInput);
        em.exports.free(ptrOutput);
        return heapOutput;
    }

    private get sampleRateRatio() {
        return this.targetSampleRate / this.sourceSampleRate;
    }

    public destroy() {
        em.exports.destroyDecoder(this.ptrDecoder);
    }
}

/**
 * Create a new decoder instance.
 *
 * @param sourceSampleRate The sample rate used by the source (websocket).
 * @param targetSampleRate The sample rate of this system.
 * @param channels The number of channels (1 for Mono, 2 for Stereo, ...).
 * @return A new decoder.
 */
export async function createDecoder(sourceSampleRate: number, targetSampleRate: number, channels: number) {
    if (!em) { em = await wasm.init(); }
    return new Decoder(sourceSampleRate, targetSampleRate, channels);
}
