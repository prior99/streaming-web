import * as React from "react";
import * as ReactDOM from "react-dom";
import { Route, Switch } from "react-router-dom";
import { Router } from "react-router";
import { createBrowserHistory } from "history";
import { Home } from "./home";
import { Help } from "./help";
import { Listen } from "./listen";
import { TSDI } from "tsdi";
import "./index.scss";

const tsdi = new TSDI();
tsdi.enableComponentScanner();

ReactDOM.render(
    <Router history={createBrowserHistory()}>
        <Switch>
            <Route path="/help" component={Help} />
            <Route path="/listen/:id" component={Listen} />
            <Route path="/" component={Home} />
        </Switch>
    </Router>,
    document.getElementById("root"),
);
