declare const BASE_URL: string;

export interface Room {
    listener: string;
    speaker: string;
}

export async function createRoom(): Promise<Room> {
    const request = await fetch(`${BASE_URL}/create`);
    return await request.json();
}
