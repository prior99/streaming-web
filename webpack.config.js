const Webpack = require('webpack');
const path = require('path');

module.exports = {
    mode: "development",
    entry: {
        "bundle": path.join(__dirname, "src"),
        "worker": path.join(__dirname, "src", "worker"),
    },
    context: path.join(__dirname, "dist"),
    output: {
        path: path.join(__dirname, "dist"),
        filename: "[name].js",
        publicPath: "/"
    },
    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx", ".json", ".cpp"],
    },
    module: {
        rules: [
            {
                test: /\.(woff|ttf)$/,
                exclude: /node_modules/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'
                }
            }, {
                test: /\.tsx?/,
                loader: "ts-loader",
                exclude: [
                    /__tests__/,
                ],
            }, {
                test: /\.cpp$/,
                use: {
                    loader: 'cpp-wasm-loader',
                    options: {
                        emccFlags: flags => [
                            ...flags,
                            "-O0",
                            "-std=c++11",
                            "-I../../libsamplerate/src",
                            "-I../../opus/include",
                            "-L../../opus/.libs",
                            "-L../../libsamplerate/src/.libs",
                            "-lopus",
                            "-lsamplerate",
                        ],
                        wasm: true,
                        fullEnv: true,
                    }
                }
            }, {
                test: /\.scss$/,
                use: [
                    { loader: "style-loader" },
                    {
                        loader: "css-loader",
                        options: {
                            modules: true,
                            localIdentName: "[name]_[local]_[hash:base64:5]",
                            sourceMap: true,
                        },
                    },
                    { loader: "resolve-url-loader" },
                    { loader: "sass-loader" },
                ],
            },
        ],
    },
    devtool: "source-map",
    devServer: {
        port: 3020,
        historyApiFallback: true,
        contentBase: path.join(__dirname, "dist")
    },
};
